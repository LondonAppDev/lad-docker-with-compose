FROM docker:19
LABEL maintainer="mark@londonappdeveloper.com"

RUN apk add --update --no-cache python py-pip
RUN apk add --update --no-cache --virtual .tmp-deps \
      python-dev libffi-dev openssl-dev gcc libc-dev make
RUN pip install docker-compose
RUN apk del .tmp-deps
